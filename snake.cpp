/*
 *this is test file maye;
 *
 * */
#include<stdio.h>
#include<graphics.h>
#include<conio.h>
#include<mmsystem.h>
#pragma comment(lib,"winmm.lib")
#define WIN_WIDTH 640 //窗口宽高
#define WIN_HEIGHT 480
#define MAX_SNAKE 10000 //蛇的最大节数
enum DIR //蛇的方向
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
};
//每一节蛇的位置信息
struct Pos
{
	int x;
	int y;
	DWORD color;
};
struct Snake_tlg //蛇的结构体
{
	int num;
	int dir;//蛇的方向
	int score;//分数
	int size;//蛇的宽度和高度
	int speed;//移动速度
	Pos coor[MAX_SNAKE];
}snake;
struct Food_tlg
{
	int x;
	int y;
	int r;
	int flag;
	DWORD color;
}food;
//画眼睛函数
void DrawEye(int x,int y)
{
	setfillcolor(WHITE);
	solidcircle(x, y, 3);
	setfillcolor(BLACK);
	solidcircle(x, y, 2);
}
void GameInit()
{
	srand(GetTickCount());
	//初始化蛇
	snake.num = 10;
	snake.dir = RIGHT;
	snake.score = 0;
	snake.size = 10;
	snake.speed = 1;
	snake.coor[0].x = 20;
	snake.coor[0].y = 10;
	for (int i = 0; i < snake.num ; i++)
	{
		snake.coor[i].color = GREEN;
	}
	//初始化食物
	food.x = rand() % (WIN_WIDTH - 10);
	food.y = rand() % (WIN_HEIGHT - 10);
	food.flag = 1;
	food.r = rand() % 3 + 2;
	food.color = RGB(rand() % 256, rand() % 256, rand() % 256);
	mciSendString("open snake_bgm.mp3 alias a", 0, 0, 0);
	mciSendString("play a repeat", 0, 0, 0);
}
void GameDraw()
{
	//设置背景颜色
	setbkcolor(RGB(191, 215, 215));
	cleardevice();
	//画蛇 
	for (int i = 0; i < snake.num; i++)
	{
		setfillcolor(snake.coor[i].color);
		solidcircle(snake.coor[i].x, snake.coor[i].y, snake.size/2);
	}
	//画眼睛
	int eyewidth=5;
	switch (snake.dir)
	{
	case UP:
	case DOWN:
		DrawEye(snake.coor[0].x - eyewidth, snake.coor[0].y);
		DrawEye(snake.coor[0].x + eyewidth, snake.coor[0].y);
		break;
	case LEFT:	
	case RIGHT:
		DrawEye(snake.coor[0].x, snake.coor[0].y - eyewidth);
		DrawEye(snake.coor[0].x, snake.coor[0].y + eyewidth);
		break;
	}
	//画食物
	if (food.flag == 1)
	{
		setfillcolor(food.color);
		solidcircle(food.x, food.y, food.r);
	}
	//显示分数
	char temp[20] = "";
	sprintf(temp, "分数:%d", snake.score);
	setbkmode(TRANSPARENT);
	outtextxy(20, 20, temp);
}
void SnakeMove()
{
	for (int i = snake.num - 1; i > 0; i--)//从最后一节蛇开始，每一节蛇都等于前一节蛇的上一次坐标
	{
		snake.coor[i].x = snake.coor[i - 1].x;
		snake.coor[i].y = snake.coor[i - 1].y;
	}
	//根据方向移动
	switch (snake.dir)
	{
	case UP:
		snake.coor[0].y -= snake.speed;
		if (snake.coor[0].y + 10 <= 0)
		{
			snake.coor[0].y = WIN_HEIGHT;
		}
		break;
	case DOWN:
		snake.coor[0].y += snake.speed;
		if (snake.coor[0].y - 10 >= WIN_HEIGHT)
		{
			snake.coor[0].y = 0;
		}
		break;
	case LEFT:
		snake.coor[0].x -= snake.speed;
		if (snake.coor[0].x + 10 <= 0)
		{
			snake.coor[0].x = WIN_WIDTH;
		}
		break;
	case RIGHT:
		snake.coor[0].x += snake.speed;
		if (snake.coor[0].x - 10 >= WIN_WIDTH)
		{
			snake.coor[0].x = 0;
		}
		break;
	}

}
void KeyControl()
{
	//使用win32API获取键盘消息
	if (GetAsyncKeyState(VK_UP) && snake.dir != DOWN)
	{
		snake.dir = UP;
	}
	if (GetAsyncKeyState(VK_DOWN) && snake.dir != UP)
	{
		snake.dir = DOWN;
	}
	if (GetAsyncKeyState(VK_LEFT) && snake.dir != RIGHT)
	{
		snake.dir = LEFT;
	}
	if (GetAsyncKeyState(VK_RIGHT) && snake.dir != LEFT)
	{
		snake.dir = RIGHT;
	}
	//按空格键加速,不按回复初始状态
	if (GetAsyncKeyState(VK_SPACE) && snake.speed==1)
	{
		snake.speed = 5;
	}
	else if (snake.speed == 5)
	{
		snake.speed = 1;
	}
}
void EatFood()
{
	if (snake.coor[0].x >= food.x-food.r && snake.coor[0].x<=food.x+ food.r &&
		snake.coor[0].y >= food.y- food.r && snake.coor[0].y<=food.y+ food.r &&
		food.flag == 1)
	{
		mciSendString("close eat", 0, 0, 0);
		mciSendString("open eatfood.mp3 alias eat", 0, 0, 0);
		mciSendString("play eat", 0, 0, 0);
		snake.num+=5;
		snake.score += 10;
		food.flag = 0;
		//每一节的颜色都要初始化
		for (int i = snake.num-5; i < snake.num; i++)
		{
			snake.coor[i].color = food.color;
		}	
	}
	if (food.flag == 0)
	{
		food.x = rand() % (WIN_WIDTH - 10);
		food.y = rand() % (WIN_HEIGHT - 10);
		food.flag = 1;
		food.r = rand() % 10 + 3;
		food.color = RGB(rand() % 256, rand() % 256, rand() % 256);
	}
}
//游戏结束判断
void DontEatSelf()
{
	for (int i = 4; i < snake.num; i++)
	{
		if (snake.coor[0].x == snake.coor[i].x && snake.coor[0].y == snake.coor[i].y)
		{
			MessageBox(GetHWnd(), "Game Over!", "提示：", MB_OKCANCEL);
			exit(666);
		}
	}
}
int main()
{
	initgraph(WIN_WIDTH, WIN_HEIGHT);//初始化一个图形窗口
	GameInit();
	DWORD t1, t2;
	
	t1 = t2 = GetTickCount();
	BeginBatchDraw();
	while (1)
	{
		if (t2 - t1 > 10)
		{
			SnakeMove();
			t1 = t2;
		}
		t2 = GetTickCount();
		GameDraw();
		EatFood();
		FlushBatchDraw();
		KeyControl();
		//DontEatSelf();
	}
	//程序一闪而过怎么回事？程序运行结束，关闭窗口不是正常的吗？
	//getchar();
	closegraph();
	return 0;
}


